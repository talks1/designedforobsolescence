# Designed for Obsolescence / Changeg By Addition

Software needs to be able to evolve.
We should expect software components to ultimately be found to be
- buggy
- not providing the required functionality
- not customized for a speak use case
- not aligned with other changes we want to make

In all these situations we would like to be able to switch out that functionality and replace.

In a lot of systems this change can be difficult.

- Need to replace a signifant portion of the application to deploy a change
- Re run all tests
- Worry about how to roll back if the new version is found to cause additional issues.

What techniques allow better software evolution.

## Micro Service 

Part of the value in a microservice architecture is that we can add new services into the landscape.
As new services appear in the registry they can replace calls to older components.

### Mechanics of doing this.

- Use a microservice framework
- [Moleculer](https://moleculer.services/docs/0.14/broker.html)

These framework compose a set of services that can have independent life cycles.  A service discovery and registry system is required.

- [Consul](https://www.consul.io/use-cases/service-discovery-and-health-checking)
- [Netflix Euraka](https://github.com/Netflix/eureka#:~:text=Eureka%20is%20a%20REST%20(Representational,in%20mid%2Dtier%20load%20balancing.)

## Kafka Style Queues

Microservices generally find each other via a service registry.

Another alternative is to send all communication through queues which can be read by multiple consumers organized into groups.
We can add new consumers into a consumer group and tear down older versions.

## Decentralized Messages

Techniques like WebMvc allow discovery and direct communication between 

## Pipelines

The ideal is going to be a pipeline where we can decouple pipeline steps through a routing mechanism.

Decoupling can be acheived by pipes/event streams but how to get the events across to new versions of things.

## Browser reload as an obsolescence mechanism

Browsers happily are able reload as new versions of source code appears.
This offers one model of replacement where we replace components on a server and they are reloaded by users on a regular basis.
We can make a component obsolete by replacing it on a server.

It is concievable to have a current and next version and action of adding next moves versions around.


So the paradigm is

## CI/CD

Here the idea is to pull new versions in via a continual integration approach.
We should have a project where all you can do is change version numbers.
If you increment a version number and there are build failures the commit is not merged otherwise it is merged.
What does a project look like that only has a minimum source to bootstrap the app.

Let say we have a pipeline project which connects to a data source to produce a stream, builds a pipeline which process the stream.
We could have a project with very limited dependencies (connectors, pipeline) however in order to change the project we would need to produce new connectors, pipeline for every change within those.
An alternative is a project which pulls in all contained modules and pushes them into composition tree.
Effectively we want all replaceable parts defined in the package and sub packages should receive all dependencies by injection.

We can then increment each part via CI/CD by incrementing its library version.
This should be workable.